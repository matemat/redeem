#!/usr/bin/env python
"""
A temperature sensor class for Replicape.
This represents NTC and PTC sensors like thermistors, thermocouples
and PT100

The code borrows heavily from smoothieware which is available at:
https://github.com/Smoothieware/Smoothieware.
It was originally written by wolfmanjm


Author: Elias Bakken, Florian Hochstrasser
email: elias(dot)bakken(at)gmail(dot)com, thisis(at)parate(dot)ch
Website: http://www.thing-printer.com
License: GNU GPL v3: http://www.gnu.org/copyleft/gpl.html

 Redeem is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Redeem is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Redeem.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import math
import logging
from threading import Lock
import sys
import TemperatureSensorConfigs
from Alarm import Alarm
from MAX6675.MAX6675 import MAX6675
import traceback

class TemperatureSensor(object):
    mutex = Lock()

    @staticmethod
    def new_sensor(pin, name, sensorIdentifier):
        # Find matching entry in sensor tables and instantiate corresponding sensor
        for s in TemperatureSensorConfigs.thermistors_shh:
            if s[0] == sensorIdentifier:
                return Thermistor(pin, s, name)

        for s in TemperatureSensorConfigs.pt100:
            if s[0] == sensorIdentifier:
                logging.warning("PT100 temperature sensor support is experimental at this stage.")
                """ Experimental solution """
                return PT100(pin, s, name)

        for s in TemperatureSensorConfigs.tboard:
            if s[0] == sensorIdentifier:
                logging.warning("Tboard sensors are experimental")
                """ Not working yet. No known hardware solution """
                return Tboard(pin, s, name)

        for s in TemperatureSensorConfigs.tc_MAX6675:
            if s[0] == sensorIdentifier:
                logging.warning("Thermocouple SPI sensors are experimental")
                return TC_MAX6675(pin, s, name)

        for s in TemperatureSensorConfigs.one_wire:
            if s[0] == sensorIdentifier:
                logging.warning("One wire sensors are experimental")
                return OneWireTemp(pin, s, name)

        logging.error("The specified temperature sensor {0} is not implemented. \
You may add its config in TemperatureSensorConfigs.".format(sensorIdentifier))
        return None

    def __init__(self, pin, name):
        self.maxAdc = 4095.0
        self.pin = pin
        self.name = name

    def read_adc(self):
        """
        Reads the adc pin and returns the actual voltage value
        Returns -1 if the reading is out of range.
        """

        voltage = np.nan

        TemperatureSensor.mutex.acquire()
        try:
            with open(self.pin, "r") as file:
                signal = float(file.read().rstrip())
                if signal > self.maxAdc or signal <= 0.0:
                    voltage = -1.0
                else:
                    voltage = signal / self.maxAdc * 1.8  # input range is 0 ... 1.8V
        except IOError as e:
            Alarm(Alarm.TEMP_SENSOR_ERROR, "Unable to get ADC value on pin {0} ({1}): {2}".format(self.pin, e.errno, e.strerror))

        TemperatureSensor.mutex.release()
        return voltage

    def get_temperature(self):
        raise NotImplementedError


class Thermistor(TemperatureSensor):
    """
    This class represents standard thermistor sensors.
    It borrows heavily from Smoothieware's code (https://github.com/Smoothieware/Smoothieware).
    wolfmanjm, thanks!
    """

    def __init__(self, pin, sensorConfiguration, name):
        super(Thermistor, self).__init__(pin, name)

        """ Init """
        if len(sensorConfiguration) != 5:
            Alarm(Alarm.TEMP_SENSOR_ERROR, "Sensor configuration for {0} is missing parameters. Expected: 5, received: {1}.".format(pin, len(sensorConfiguration)))
        else:
            self.sensorIdentifier = sensorConfiguration[0] # The identifier
            self.r1 = sensorConfiguration[1]    #pullup resistance
            self.c1 = sensorConfiguration[2]    #Steinhart-Hart coefficient
            self.c2 = sensorConfiguration[3]    #Steinhart-Hart coefficient
            self.c3 = sensorConfiguration[4]    #Steinhart-Hart coefficient
            logging.debug("Initialized temperature sensor at {0} (type: {1}). Pullup value = {2} Ohm. Steinhart-Hart coefficients: c1 = {3}, c2 = {4}, c3 = {5}.".format(pin, sensorConfiguration[0], sensorConfiguration[1], sensorConfiguration[2], sensorConfiguration[3],sensorConfiguration[4]))

    def get_temperature(self):
        """ Return the temperature in degrees celsius. Uses Steinhart-Hart """
        voltage = self.read_adc()
        r = self.voltage_to_resistance(voltage)
        #if self.name == "MOSFET E":
        #    logging.debug("Voltage: "+str(voltage))
        #    logging.debug("resistance: "+str(r))
        if r > 0:
            l = math.log(r)
            t = float((1.0 / (self.c1 + self.c2 * l + self.c3 * math.pow(l,3))) - 273.15)
        else:
            t = -273.15
            logging.debug("Reading sensor {0} on {1}, but it seems to be out of bounds. R is {2}. Setting temp to {3}.".format(self.sensorIdentifier, self.pin,r,t))
        return max(t, 0.0) # Cap it at 0

    def voltage_to_resistance(self,voltage):
        """ Convert the voltage to a resistance value """
        if voltage == 0 or (abs(voltage - 1.8) < 0.0001):
            return 10000000.0
        return self.r1 / ((1.8 / voltage) - 1.0)


class PT100(TemperatureSensor):
    """
    This class represents PT100 temperature sensors
    Caution: This code is experimental.
    
    """

    def __init__(self, pin, sensorConfiguration, name):
        super(PT100, self).__init__(pin, name)

        if len(sensorConfiguration) != 5:
            logging.error("PT100 Sensor configuration for {0} is missing parameters. Expected: 5, received: {1}.".format(pin, len(sensorConfiguration)))    
            Alarm(Alarm.TEMP_SENSOR_ERROR, "PT100 Sensor configuration for {0} is missing parameters. Expected: 5, received: {1}.".format(pin, len(sensorConfiguration)))
        else:
            self.sensorIdentifier = sensorConfiguration[0] # The identifier
            self.pullup = sensorConfiguration[1]
            self.R0 = sensorConfiguration[2]
            self.A  = sensorConfiguration[3]
            self.B  = sensorConfiguration[4]

    # The following calculations are based on the PT100 connected in the same as as a thermistor.
    # Connecting it this way will give very low accuracy, but better accuracy require hardware modification.
    def voltage_to_resistance(self,voltage):

        """ Convert the voltage to a resistance value """
        if voltage == 0 or (abs(voltage - 1.8) < 0.0001):
            return 10000000.0
        return self.pullup / ((1.8 / voltage) - 1.0)

    def get_temperature(self):
        """ Return the temperature in degrees celsius. """
        voltage = self.read_adc()
        r = self.voltage_to_resistance(voltage)
        return (-self.A + np.sqrt(self.A**2 - 4*self.B*(1-r/self.R0 )))/(2*self.B)


class TC_MAX6675(TemperatureSensor):
    """
    This class represents a MAX6675 thermocouple board, communicating over SPI
    Caution: This code is experimental.

    """
    SPI_mutex = Lock()

    def __init__(self, pin, sensorConfiguration, name):
        super(TC_MAX6675, self).__init__(pin, name)

        exp_len = 4
        if len(sensorConfiguration) != exp_len:
            logging.error("MAX6675 sensor configuration for {0}, is missing parameters. Expected: {1}, received: {2}.".format(pin, exp_len, len(sensorConfiguration)))    
            Alarm(Alarm.TEMP_SENSOR_ERROR, "MAX6675 sensor configuration for {0} is missing parameters. Expected: {1}, received: {2}.".format(pin, exp_len, len(sensorConfiguration)))
        else:
            self.sensorIdentifier = sensorConfiguration[0]  # The identifier
            self.pin_CLK = sensorConfiguration[1]
            self.pin_CS = sensorConfiguration[2]
            self.pin_DO = sensorConfiguration[3]
        self.SPI_class = MAX6675(self.pin_CLK, self.pin_CS, self.pin_DO)

    def get_temperature(self):
        """ Return the temperature in degrees celsius. """
        TC_MAX6675.SPI_mutex.acquire()  # to prevent two devices fighting over the same SPI
        temp = self.SPI_class.readTempC()
        TC_MAX6675.SPI_mutex.release()
        if np.isnan(temp):
            Alarm(Alarm.TEMP_SENSOR_ERROR, "MAX6675 temperature sensor for {} has been disconnected".format(self.name))
        return temp
        # temp = self.SPI_class.readTempC()
        # if np.isnan(temp):
        #     return 666.0 # to trigger the fast rise alarm of Octoprint etc.
        # else:
        #     return temp


class OneWireTemp(TemperatureSensor):
    """
    This class represents a MAX6675 thermocouple board, communicating over SPI
    Caution: This code is experimental.

    """

    def __init__(self, pin, sensorConfiguration, name):
        super(OneWireTemp, self).__init__(pin, name)

        exp_len = 2
        if len(sensorConfiguration) != exp_len:
            logging.error("One wire temp. sensor configuration for {0}, is missing parameters. Expected: {1}, received: {2}.".format(pin, exp_len, len(sensorConfiguration)))
            Alarm(Alarm.TEMP_SENSOR_ERROR, "One wire temp. sensor configuration for {0} is missing parameters. Expected: {1}, received: {2}.".format(pin, exp_len, len(sensorConfiguration)))
        else:
            self.sensorIdentifier = sensorConfiguration[0]  # The identifier

    def get_temperature(self):
        """ Return the temperature in degrees celsius. """
        temp = np.nan
        with open(self.pin, "r") as f:
            try:
                temp = float(f.read().split("t=")[-1])/1000.0
            except IOError:
                Alarm(Alarm.TEMP_SENSOR_ERROR, "1-wire temperature sensor for {} has been disconnected".format(self.name))
        return temp


class Tboard (TemperatureSensor):
    """
    Tboard returns a linear temp of 5mv/deg C
    """

    def __init__(self, pin, sensorConfiguration, name):
        super(Tboard, self).__init__(pin, name)

        self.voltage_pr_degree = float(sensorConfiguration[1])
        logging.debug("Initialized temperature sensor at {0} with temp/deg = {1}".format(pin, sensorConfiguration[1]))

    def get_temperature(self):
        voltage = self.read_adc()
        return voltage/self.voltage_pr_degree
